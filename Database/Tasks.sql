﻿CREATE TABLE [dbo].[Tasks]
(
    [Id] INT NOT NULL PRIMARY KEY IDENTITY,
    [UserId] INT NOT NULL,
    [Title] NVARCHAR(200) NOT NULL, 
    [Description] NVARCHAR(2000) NULL, 
    [CreateDate] DATETIME2 NOT NULL, 
    [CompleteDate] DATETIME2 NULL, 
    [ChangeDate] DATETIME2 NOT NULL,
    CONSTRAINT [FK_Tasks_Users] FOREIGN KEY ([UserId]) REFERENCES [Users]([Id])
)
