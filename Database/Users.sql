﻿CREATE TABLE [dbo].[Users]
(
    [Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Login] NVARCHAR(100) NOT NULL, 
    [PasswordHash] NVARCHAR(200) NOT NULL, 
    [CreateDate] DATETIME2 NOT NULL, 
    [ChangeDate] DATETIME2 NOT NULL
)
