﻿using FluentAssertions;
using NUnit.Framework;
using Sudakov.TaskManager.Domain.Tasks;
using Sudakov.TaskManager.Domain.Users;
using System.Collections.Generic;
using System.Linq;

namespace Sudakov.TaskManager.Repositories.Sql.EF.Tests
{
    [TestFixture]
    public class TasksRepositoryTests : RepositoryTestsBase
    {
        private Task _task;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            var user = TestData.CreateDefaultUser();
            Commit<IUsersRepository>(x => x.Add(user));

            _task = TestData.CreateDefaultTask();
            _task.UserId = user.Id;
        }

        [Test]
        public void CanAdd()
        {
            Commit<ITasksRepository>(x => x.Add(_task));

            var dbTask = Fetch<ITasksRepository, Task>(x => x.FindBy(_task.Id));
            dbTask.Should().BeEquivalentTo(_task);
        }

        [Test]
        public void CanSave()
        {
            Commit<ITasksRepository>(x => x.Add(_task));

            _task.Title = "NewTestTitle";
            Commit<ITasksRepository>(x => x.Save(_task));

            var dbTask = Fetch<ITasksRepository, Task>(x => x.FindBy(_task.Id));
            dbTask.Should().BeEquivalentTo(_task);
        }

        [Test]
        public void CanFindAllByUser()
        {
            Commit<ITasksRepository>(x => x.Add(_task));

            var dbUserTasks = Fetch<ITasksRepository, IEnumerable<Task>>(x => x.FindAll(_task.UserId).ToList());
            dbUserTasks.Should().HaveCount(1);
        }

        protected override void ClearAll()
        {
            base.ClearAll();
            Clear<ITasksRepository, Task>();
            Clear<IUsersRepository, User>();
        }
    }
}
