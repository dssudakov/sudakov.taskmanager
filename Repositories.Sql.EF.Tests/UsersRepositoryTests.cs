﻿using FluentAssertions;
using NUnit.Framework;
using Sudakov.TaskManager.Domain.Users;
using System.Collections.Generic;
using System.Linq;

namespace Sudakov.TaskManager.Repositories.Sql.EF.Tests
{
    [TestFixture]
    public class UsersRepositoryTests : RepositoryTestsBase
    {
        private User _user;

        [SetUp]
        public override  void SetUp()
        {
            base.SetUp();
            _user = TestData.CreateDefaultUser();
        }

        [Test]
        public void CanAdd()
        {
            Commit<IUsersRepository>(x => x.Add(_user));

            var dbUser = Fetch<IUsersRepository, User>(x => x.FindBy(_user.Id));
            dbUser.Should().BeEquivalentTo(_user);
        }

        [Test]
        public void CanSave()
        {
            Commit<IUsersRepository>(x => x.Add(_user));

            _user.Login = "TestLogin002";
            Commit<IUsersRepository>(x => x.Save(_user));

            var dbUser = Fetch<IUsersRepository, User>(x => x.FindBy(_user.Id));
            dbUser.Should().BeEquivalentTo(_user);
        }

        [Test]
        public void CanFindAll()
        {
            Commit<IUsersRepository>(x => x.Add(_user));

            var dbUsers = Fetch<IUsersRepository, IEnumerable<User>>(x => x.FindAll().ToList());
            dbUsers.Should().HaveCount(1);
        }

        [Test]
        public void CanFindByLogin()
        {
            Commit<IUsersRepository>(x => x.Add(_user));

            var dbUser = Fetch<IUsersRepository, User>(x => x.FindBy(_user.Login));
            dbUser.Should().BeEquivalentTo(_user);
        }

        protected override void ClearAll()
        {
            base.ClearAll();
            Clear<IUsersRepository, User>();
        }
    }
}
