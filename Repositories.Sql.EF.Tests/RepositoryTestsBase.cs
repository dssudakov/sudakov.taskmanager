﻿using NUnit.Framework;
using Sudakov.TaskManager.Infrastructure.DomainBase;
using Sudakov.TaskManager.Infrastructure.Repositories;
using Sudakov.TaskManager.Infrastructure.UnitOfWork;
using Sudakov.TaskManager.Repositories.Sql.EF.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Sudakov.TaskManager.Repositories.Sql.EF.Tests
{
    [TestFixture]
    public abstract class RepositoryTestsBase
    {
        private IUnitOfWorkFactory _unitOfWorkFactory;

        [SetUp]
        public virtual void SetUp()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["TaskManager.Tests"].ConnectionString;
            _unitOfWorkFactory = new EFUnitOfWorkFactory(connectionString);
            ClearAll();
        }

        [TearDown]
        public virtual void TearDown()
        {
            ClearAll();
        }

        protected virtual void ClearAll()
        {
        }

        protected void Clear<TRepository, TEntity>()
            where TRepository : IRepository<TEntity>
            where TEntity : IAggregateRoot
        {
            var entities = Fetch<TRepository, List<TEntity>>(x => x.FindAll().ToList());
            Commit<TRepository>(x => entities.ForEach(e => x.Remove(e)));
        }

        protected void Commit<TRepository>(Action<TRepository> action)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                action(unitOfWork.GetRepository<TRepository>());
                unitOfWork.Commit();
            }
        }

        protected TResult Fetch<TRepository, TResult>(Func<TRepository, TResult> func)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                return func(unitOfWork.GetRepository<TRepository>());
            }
        }
    }
}
