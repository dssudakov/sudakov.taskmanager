﻿using Sudakov.TaskManager.Domain.Tasks;
using Sudakov.TaskManager.Domain.Users;
using System;

namespace Sudakov.TaskManager.Repositories.Sql.EF.Tests
{
    public static class TestData
    {
        public static User CreateDefaultUser()
        {
            return new User
            {
                Login = "TestLogin",
                PasswordHash = "TestPasswordHash",
                CreateDate = DateTime.Now,
                ChangeDate = DateTime.Now
            };
        }

        public static Task CreateDefaultTask()
        {
            return new Task
            {
                Title = "TestTitle",
                Description = "TestDescription",
                CreateDate = DateTime.Now,
                ChangeDate = DateTime.Now
            };
        }
    }
}
