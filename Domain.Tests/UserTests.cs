﻿using FluentAssertions;
using NUnit.Framework;
using Sudakov.TaskManager.Domain.Users;

namespace Sudakov.TaskManager.Domain.Tests
{
    [TestFixture]
    public class UserTests
    {
        private const string LOGIN = "TestLogin";
        private const string CORRECT_PASSWORD = "TestPassword";
        private const string CORRECT_PASSWORD_HASH = "5DA8FD0E50465911CA6B33383189C6B6CBB024B62B3FF282A15C44034DACDC134FCC057C3F17F341702C6EFC32D08FBDA8DE8764AB1454A6B6223333CD490CF1";
        private const string INCORRECT_PASSWORD = "IncorrectPassword";

        [Test]
        public void SetPassword_SetsPasswordHash()
        {
            var user = new User
            {
                Login = LOGIN
            };
            user.SetPassword(CORRECT_PASSWORD);
            user.PasswordHash.Should().Be(CORRECT_PASSWORD_HASH);
        }

        [Test]
        [TestCase(CORRECT_PASSWORD, ExpectedResult = true)]
        [TestCase(INCORRECT_PASSWORD, ExpectedResult = false)]
        public bool VerifyPassword_ReturnsVerificationResult(string password)
        {
            var user = new User
            {
                Login = LOGIN,
                PasswordHash = CORRECT_PASSWORD_HASH
            };
            return user.VerifyPassword(password);
        }
    }
}
