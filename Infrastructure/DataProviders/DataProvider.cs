﻿using Sudakov.TaskManager.Infrastructure.DomainBase;
using Sudakov.TaskManager.Infrastructure.Persistence;
using Sudakov.TaskManager.Infrastructure.Repositories;
using System.Collections.Generic;

namespace Sudakov.TaskManager.Infrastructure.DataProviders
{
    public class DataProvider<TRepository, TEntity> : IDataProvider<TRepository, TEntity>
        where TRepository : IRepository<TEntity>
        where TEntity : IAggregateRoot
    {
        private readonly IPersistenceService _persistenceService;

        public IPersistenceService PersistenceService => _persistenceService;

        public DataProvider(IPersistenceService persistenceService)
        {
            _persistenceService = persistenceService;
        }

        public IEnumerable<TEntity> FindAll()
        {
            return _persistenceService.GetMany(x => x.GetRepository<TRepository>().FindAll());
        }

        public TEntity FindBy(int id)
        {
            return _persistenceService.Get(x => x.GetRepository<TRepository>().FindBy(id));
        }

        public void Add(TEntity entity)
        {
            _persistenceService.Commit(x => x.GetRepository<TRepository>().Add(entity));
        }

        public void Save(TEntity entity)
        {
            _persistenceService.Commit(x => x.GetRepository<TRepository>().Save(entity));
        }

        public void Remove(TEntity entity)
        {
            _persistenceService.Commit(x => x.GetRepository<TRepository>().Remove(entity));
        }
    }
}
