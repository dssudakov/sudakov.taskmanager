﻿using Sudakov.TaskManager.Infrastructure.DomainBase;
using Sudakov.TaskManager.Infrastructure.Repositories;
using System.Collections.Generic;

namespace Sudakov.TaskManager.Infrastructure.DataProviders
{
    public interface IDataProvider<TRepository, TEntity>
        where TRepository : IRepository<TEntity>
        where TEntity : IAggregateRoot
    {
        IEnumerable<TEntity> FindAll();
        TEntity FindBy(int id);
        void Add(TEntity entity);
        void Save(TEntity entity);
        void Remove(TEntity entity);
    }
}
