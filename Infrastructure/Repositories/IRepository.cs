﻿using Sudakov.TaskManager.Infrastructure.DomainBase;
using Sudakov.TaskManager.Infrastructure.UnitOfWork;
using System.Linq;

namespace Sudakov.TaskManager.Infrastructure.Repositories
{
    public interface IRepository
    {
        IUnitOfWork UnitOfWork { get; }
    }

    public interface IRepository<TEntity> : IRepository
        where TEntity : IAggregateRoot
    {
        IQueryable<TEntity> FindAll();
        TEntity FindBy(int key);
        void Add(TEntity entity);
        void Save(TEntity entity);
        void Remove(TEntity entity);
    }
}
