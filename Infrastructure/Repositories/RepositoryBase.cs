﻿using Sudakov.TaskManager.Infrastructure.DomainBase;
using Sudakov.TaskManager.Infrastructure.UnitOfWork;
using System.Linq;

namespace Sudakov.TaskManager.Infrastructure.Repositories
{
    public abstract class RepositoryBase<TEntity> : IRepository<TEntity>
        where TEntity : IAggregateRoot
    {
        public IUnitOfWork UnitOfWork { get; private set; }

        public RepositoryBase(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        public abstract IQueryable<TEntity> FindAll();
        public abstract TEntity FindBy(int key);

        public void Add(TEntity entity)
        {
            UnitOfWork.RegisterAdded(entity);
        }

        public void Save(TEntity entity)
        {
            UnitOfWork.RegisterChanged(entity);
        }

        public void Remove(TEntity entity)
        {
            UnitOfWork.RegisterRemoved(entity);
        }
    }
}
