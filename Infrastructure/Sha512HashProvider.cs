﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Sudakov.TaskManager.Infrastructure
{
    public class Sha512HashProvider
    {
        public string ComputeHash(string data)
        {
            using (var sha512Provider = new SHA512CryptoServiceProvider())
            {
                var dataBytes = Encoding.UTF8.GetBytes(data);
                var hashBytes = sha512Provider.ComputeHash(dataBytes);
                var hash = BitConverter.ToString(hashBytes).Replace("-", "");

                return hash;
            }
        }
    }
}
