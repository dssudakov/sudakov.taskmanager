﻿using Sudakov.TaskManager.Infrastructure.DomainBase;
using System;

namespace Sudakov.TaskManager.Infrastructure.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        void RegisterAdded(IAggregateRoot entity);
        void RegisterChanged(IAggregateRoot entity);
        void RegisterRemoved(IAggregateRoot entity);
        void Commit();
        void Rollback();
        T GetRepository<T>();
    }
}
