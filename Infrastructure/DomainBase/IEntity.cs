﻿namespace Sudakov.TaskManager.Infrastructure.DomainBase
{
    public interface IEntity
    {
        int Key { get; }
    }
}
