﻿using Sudakov.TaskManager.Infrastructure.UnitOfWork;
using System;
using System.Collections.Generic;

namespace Sudakov.TaskManager.Infrastructure.Persistence
{
    public interface IPersistenceService
    {
        TResult Get<TResult>(Func<IUnitOfWork, TResult> func);
        IEnumerable<TResult> GetMany<TResult>(Func<IUnitOfWork, IEnumerable<TResult>> func);
        void Commit(Action<IUnitOfWork> action);
    }
}
