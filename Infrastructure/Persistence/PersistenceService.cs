﻿using Sudakov.TaskManager.Infrastructure.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Sudakov.TaskManager.Infrastructure.Persistence
{
    public class PersistenceService : IPersistenceService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public PersistenceService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public TResult Get<TResult>(Func<IUnitOfWork, TResult> func)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                return func(unitOfWork);
            }
        }

        public IEnumerable<TResult> GetMany<TResult>(Func<IUnitOfWork, IEnumerable<TResult>> func)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                return func(unitOfWork).ToList();
            }
        }

        public void Commit(Action<IUnitOfWork> action)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                action(unitOfWork);
                unitOfWork.Commit();
            }
        }
    }
}
