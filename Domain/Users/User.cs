﻿using Sudakov.TaskManager.Infrastructure;
using Sudakov.TaskManager.Infrastructure.DomainBase;
using System;

namespace Sudakov.TaskManager.Domain.Users
{
    public class User : IAggregateRoot
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string PasswordHash { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ChangeDate { get; set; }

        public int Key => Id;

        public void SetPassword(string password)
        {
            PasswordHash = GetPasswordHash(password);
        }

        public bool VerifyPassword(string password)
        {
            return PasswordHash == GetPasswordHash(password);
        }

        private string GetPasswordHash(string password)
        {
            return new Sha512HashProvider().ComputeHash(Login + password);
        }
    }
}
