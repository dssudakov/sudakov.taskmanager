﻿using Sudakov.TaskManager.Infrastructure.DataProviders;

namespace Sudakov.TaskManager.Domain.Users
{
    public interface IUsersProvider : IDataProvider<IUsersRepository, User>
    {
        User FindBy(string login);
    }
}
