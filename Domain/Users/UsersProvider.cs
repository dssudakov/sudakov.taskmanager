﻿using Sudakov.TaskManager.Infrastructure.DataProviders;
using Sudakov.TaskManager.Infrastructure.Persistence;

namespace Sudakov.TaskManager.Domain.Users
{
    public class UsersProvider : DataProvider<IUsersRepository, User>, IUsersProvider
    {
        public UsersProvider(IPersistenceService persistenceService)
            : base(persistenceService)
        {
        }

        public User FindBy(string login)
        {
            return PersistenceService.Get(x => x.GetRepository<IUsersRepository>().FindBy(login));
        }
    }
}
