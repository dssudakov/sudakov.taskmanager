﻿using Sudakov.TaskManager.Infrastructure.Repositories;

namespace Sudakov.TaskManager.Domain.Users
{
    public interface IUsersRepository : IRepository<User>
    {
        User FindBy(string login);
    }
}
