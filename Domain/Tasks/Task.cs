﻿using Sudakov.TaskManager.Infrastructure.DomainBase;
using System;

namespace Sudakov.TaskManager.Domain.Tasks
{
    public class Task : IAggregateRoot
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? CompleteDate { get; set; }
        public DateTime ChangeDate { get; set; }

        public int Key => Id;

        public Task()
        {
            CreateDate = DateTime.Now;
        }
    }
}
