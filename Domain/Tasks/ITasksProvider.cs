﻿using Sudakov.TaskManager.Infrastructure.DataProviders;
using System.Collections.Generic;

namespace Sudakov.TaskManager.Domain.Tasks
{
    public interface ITasksProvider : IDataProvider<ITasksRepository, Task>
    {
        IEnumerable<Task> FindAll(int userId);
    }
}
