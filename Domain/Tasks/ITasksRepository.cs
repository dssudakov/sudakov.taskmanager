﻿using Sudakov.TaskManager.Infrastructure.Repositories;
using System.Linq;

namespace Sudakov.TaskManager.Domain.Tasks
{
    public interface ITasksRepository : IRepository<Task>
    {
        IQueryable<Task> FindAll(int userId);
    }
}
