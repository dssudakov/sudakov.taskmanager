﻿using Sudakov.TaskManager.Infrastructure.DataProviders;
using Sudakov.TaskManager.Infrastructure.Persistence;
using System.Collections.Generic;

namespace Sudakov.TaskManager.Domain.Tasks
{
    public class TasksProvider : DataProvider<ITasksRepository, Task>, ITasksProvider
    {
        public TasksProvider(IPersistenceService persistenceService)
            : base(persistenceService)
        {
        }

        public IEnumerable<Task> FindAll(int userId)
        {
            return PersistenceService.GetMany(x => x.GetRepository<ITasksRepository>().FindAll(userId));
        }
    }
}
