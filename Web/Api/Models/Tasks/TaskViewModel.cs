﻿using Sudakov.TaskManager.Domain.Tasks;
using System;
using System.ComponentModel.DataAnnotations;

namespace Sudakov.TaskManager.Web.Api.Models.Tasks
{
    public class TaskViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsCompleted { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? CompleteDate { get; set; }
        public DateTime ChangeDate { get; set; }

        public static TaskViewModel Create(Task task)
        {
            if (task == null)
                throw new ArgumentNullException(nameof(task));

            return new TaskViewModel
            {
                Id = task.Id,
                Title = task.Title,
                Description = task.Description,
                IsCompleted = task.CompleteDate.HasValue,
                CreateDate = task.CreateDate,
                CompleteDate = task.CompleteDate,
                ChangeDate = task.ChangeDate
            };
        }

        public void UpdateEntity(Task task)
        {
            if (task == null)
                throw new ArgumentNullException(nameof(task));

            task.Title = Title;
            task.Description = Description;
            task.CompleteDate = IsCompleted ? DateTime.Now : (DateTime?)null;
            task.ChangeDate = DateTime.Now;
        }

        public Task ToEntity()
        {
            var task = new Task();
            UpdateEntity(task);

            return task;
        }
    }
}
