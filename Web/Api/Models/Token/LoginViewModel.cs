﻿using System.ComponentModel.DataAnnotations;

namespace Sudakov.TaskManager.Web.Api.Models.Token
{
    public class LoginViewModel
    {
        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
