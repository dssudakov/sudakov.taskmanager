﻿using Microsoft.AspNetCore.Mvc.Filters;
using System.Threading;

namespace Sudakov.TaskManager.Web.Api.Services
{
    public class DelayActionFilter : IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            Thread.Sleep(1000);
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
        }
    }
}
