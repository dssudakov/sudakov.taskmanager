﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Sudakov.TaskManager.Domain.Users;
using Sudakov.TaskManager.Web.Api.Models.Token;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Sudakov.TaskManager.Web.Api.Controllers
{
    [Route("api/[controller]")]
    public class TokenController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IUsersProvider _usersProvider;

        public TokenController(IConfiguration configuration, IUsersProvider usersProvider)
        {
            _configuration = configuration;
            _usersProvider = usersProvider;
        }

        [HttpPost]
        public IActionResult Issue([FromBody]LoginViewModel viewModel)
        {
            var user = _usersProvider.FindBy(viewModel.Login);
            if (user == null || !user.VerifyPassword(viewModel.Password))
            {
                return Unauthorized();
            }

            var token = CreateToken(user);
            return Ok(token);
        }

        private string CreateToken(User user)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha512);

            var token = new JwtSecurityToken(
                _configuration["Jwt:Issuer"],
                _configuration["Jwt:Issuer"],
                new List<Claim>
                {
                    new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString())
                },
                null,
                DateTime.Now.AddHours(1),
                credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
