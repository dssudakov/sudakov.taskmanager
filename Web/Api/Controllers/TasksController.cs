﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sudakov.TaskManager.Domain.Tasks;
using Sudakov.TaskManager.Web.Api.Models.Tasks;
using Sudakov.TaskManager.Web.Api.Services;
using System.Linq;

namespace Sudakov.TaskManager.Web.Api.Controllers
{
    [Route("api/tasks")]
    [Authorize]
    public class TasksController : Controller
    {
        private readonly ITasksProvider _tasksProvider;

        public TasksController(ITasksProvider tasksProvider)
        {
            _tasksProvider = tasksProvider;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var viewModels = _tasksProvider
                .FindAll(HttpContext.GetUserId())
                .OrderByDescending(x => x.CreateDate)
                .Select(TaskViewModel.Create);

            return Ok(viewModels);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var task = _tasksProvider.FindBy(id);
            if (!IsAvailable(task))
            {
                return NotFound();
            }

            return Ok(TaskViewModel.Create(task));
        }

        [HttpPost]
        public IActionResult Create([FromBody]TaskViewModel viewModel)
        {
            var task = viewModel.ToEntity();
            task.UserId = HttpContext.GetUserId();
            _tasksProvider.Add(task);

            return NoContent();
        }

        [HttpPut]
        public IActionResult Change([FromBody]TaskViewModel viewModel)
        {
            var task = _tasksProvider.FindBy(viewModel.Id);
            if (!IsAvailable(task))
            {
                return NotFound();
            }

            viewModel.UpdateEntity(task);
            _tasksProvider.Save(task);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var task = _tasksProvider.FindBy(id);
            if (!IsAvailable(task))
            {
                return NotFound();
            }

            _tasksProvider.Remove(task);

            return NoContent();
        }

        private bool IsAvailable(Task task)
        {
            return task != null && task.UserId == HttpContext.GetUserId();
        }
    }
}