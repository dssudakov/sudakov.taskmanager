﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Sudakov.TaskManager.Domain.Tasks;
using Sudakov.TaskManager.Domain.Users;
using Sudakov.TaskManager.Infrastructure.Persistence;
using Sudakov.TaskManager.Infrastructure.UnitOfWork;
using Sudakov.TaskManager.Repositories.Sql.EF.UnitOfWork;
using Sudakov.TaskManager.Web.Api.Services;
using System.IO;
using System.Net;
using System.Text;

namespace Sudakov.TaskManager.Web.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; private set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("TaskManager");
            services.AddSingleton<IUnitOfWorkFactory>(new EFUnitOfWorkFactory(connectionString));
            services.AddTransient<IPersistenceService, PersistenceService>();
            services.AddTransient<IUsersProvider, UsersProvider>();
            services.AddTransient<ITasksProvider, TasksProvider>();

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                ValidIssuer = Configuration["Jwt:Issuer"],
                ValidAudience = Configuration["Jwt:Issuer"],
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
            };
            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options => options.TokenValidationParameters = tokenValidationParameters);

            services.AddCors();
            services.AddMvc(options =>
            {
                options.Filters.Add<ValidatorActionFilter>();
                options.Filters.Add<DelayActionFilter>();
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            }

            app.Use(async (context, next) =>
            {
                await next();
                if (context.Response.StatusCode == (int)HttpStatusCode.NotFound &&
                    !Path.HasExtension(context.Request.Path) &&
                    !context.Request.Path.StartsWithSegments("/api/"))
                {
                    context.Request.Path = "/";
                    await next();
                }
            });

            app.UseAuthentication();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseMvc();
        }
    }
}
