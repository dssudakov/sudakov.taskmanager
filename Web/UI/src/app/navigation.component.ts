import { Component, OnInit } from "@angular/core";

import { AuthService } from "./auth/auth.service";
import { AuthData } from "./auth/auth-data";
import { MenuItem } from "primeng/components/common/menuitem";

@Component({
  selector: "app-navigation",
  templateUrl: "./navigation.component.html"
})
export class NavigationComponent implements OnInit {

  menuItems: MenuItem[];

  constructor(public authService: AuthService) { }

  ngOnInit(): void {
    this.menuItems = [
      { label: "Task Manager", routerLink: "/", styleClass: "logo" }
    ];
  }
}
