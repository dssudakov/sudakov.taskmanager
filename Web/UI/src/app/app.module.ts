import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { MenubarModule } from 'primeng/menubar';
import { ButtonModule } from "primeng/button";

import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation.component';
import { AppRoutingModule } from './app-routing.module';

import { AuthModule } from './auth/auth.module';
import { LoginModule } from './login/login.module';
import { TasksModule } from './tasks/tasks.module';

const AUTH_OPTIONS: AuthOptions = {
  tokenUrl: environment.apiUrl + 'token',
  loginUrl: '/login',
  mainUrl: '/'
};

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ProgressSpinnerModule,
    MenubarModule,
    ButtonModule,
    AuthModule.forRoot(AUTH_OPTIONS),
    LoginModule,
    TasksModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
