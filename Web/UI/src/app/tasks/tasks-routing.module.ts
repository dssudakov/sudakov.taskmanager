import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TaskListComponent } from './task-list.component';
import { TaskDetailComponent } from './task-detail.component';
import { AuthGuard } from '../auth/auth-guard.service';
import { TaskListResolver } from './task-list-resolver';
import { TaskDetailResolver } from './task-detail-resolver.service';

const tasksRoutes: Routes = [
  { path: 'tasks', component: TaskListComponent, canActivate: [AuthGuard], resolve: { tasks: TaskListResolver } },
  { path: 'task', component: TaskDetailComponent, canActivate: [AuthGuard], resolve: { task: TaskDetailResolver } },
  { path: 'task/:id', component: TaskDetailComponent, canActivate: [AuthGuard], resolve: { task: TaskDetailResolver } }
];

@NgModule({
  imports: [
    RouterModule.forChild(tasksRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class TasksRoutingModule { }
