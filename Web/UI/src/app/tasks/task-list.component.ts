import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Task } from './task';
import { TasksService } from './tasks.service';

@Component({
  templateUrl: './task-list.component.html'
})
export class TaskListComponent implements OnInit {

  tasks: Task[] = [];
  selectedTask: Task = null;

  constructor(private router: Router, private route: ActivatedRoute, private tasksService: TasksService) { }

  onRemove(task: Task): void {
    this.tasksService.remove(task.id).subscribe(x => this.refreshTasks());
  }

  ngOnInit() {
    this.route.data.subscribe((data: { tasks: Task[] }) => { this.tasks = data.tasks; });
  }

  private refreshTasks() {
    this.tasksService.getAll().subscribe(tasks => this.tasks = tasks);
  }

}
