export class Task {
  id: number;
  title: string;
  description?: string;
  isCompleted: boolean;
  createDate: Date;
  completeDate?: Date;
  changeDate: Date;
}
