import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Task } from './task';

const apiUrl: string = "http://localhost:44689/api/tasks";

@Injectable()
export class TasksService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Task[]> {
    return this.http.get<Task[]>(apiUrl);
  }

  getOne(id: number): Observable<Task> {
    return this.http.get<Task>(`${apiUrl}/${id}`);
  }

  add(task: Task): Observable<void> {
    return this.http.post<void>(apiUrl, task);
  }

  save(task: Task): Observable<void> {
    return this.http.put<void>(apiUrl, task);
  }

  remove(id: number): Observable<void> {
    return this.http.delete<void>(`${apiUrl}/${id}`);
  }

}
