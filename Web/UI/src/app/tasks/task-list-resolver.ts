import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs";

import { Task } from "./task";
import { TasksService } from "./tasks.service";

@Injectable()
export class TaskListResolver implements Resolve<Task[]> {

  constructor(private tasksService: TasksService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Task[]> {

    return this.tasksService.getAll();
  }
}
