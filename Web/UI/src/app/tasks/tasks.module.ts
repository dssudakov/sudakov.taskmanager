import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';

import { PanelModule } from 'primeng/panel';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputSwitchModule } from 'primeng/inputswitch';

import { TaskListComponent } from './task-list.component';
import { TaskDetailComponent } from './task-detail.component';
import { TasksService } from './tasks.service';
import { TaskListResolver } from './task-list-resolver';
import { TaskDetailResolver } from './task-detail-resolver.service';
import { TasksRoutingModule } from './tasks-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NoopAnimationsModule,
    PanelModule,
    ButtonModule,
    TableModule,
    InputTextModule,
    InputTextareaModule,
    InputSwitchModule,
    TasksRoutingModule
  ],
  declarations: [
    TaskListComponent,
    TaskDetailComponent
  ],
  providers: [
    TasksService,
    TaskListResolver,
    TaskDetailResolver
  ]
})
export class TasksModule { }
