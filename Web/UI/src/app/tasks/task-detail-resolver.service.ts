import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import { Observable, of } from "rxjs";
import { map } from "rxjs/operators";

import { Task } from "./task";
import { TasksService } from "./tasks.service";

@Injectable()
export class TaskDetailResolver implements Resolve<Task> {

  constructor(private router: Router, private tasksService: TasksService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Task> {

    const id = route.paramMap.get("id");
    if (!id) {
      return of(new Task());
    }

    return this.tasksService.getOne(+id).pipe(
      map(task => {
        if (task) {
          return task;
        }
        else {
          this.router.navigate(["../tasks"]);
          return null;
        }
      })
    );
  }
}
