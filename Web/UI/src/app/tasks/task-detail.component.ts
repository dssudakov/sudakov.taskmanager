import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Task } from './task';
import { TasksService } from './tasks.service';

@Component({
  templateUrl: './task-detail.component.html'
})
export class TaskDetailComponent implements OnInit {

  task: Task;

  constructor(private router: Router, private route: ActivatedRoute, private tasksService: TasksService) { }

  onIsCompletedToggle(): void {
    this.task.isCompleted = !this.task.isCompleted;
  }

  onSave(): void {
    if (this.task.id) {
      this.tasksService.save(this.task).subscribe(x => this.router.navigate(["../tasks"]));
    } else {
      this.tasksService.add(this.task).subscribe(x => this.router.navigate(["../tasks"]));
    }
  }

  onCancel(): void {
    this.router.navigate(["../tasks"]);
  }

  ngOnInit() {
    this.route.data.subscribe((data: { task: Task }) => { this.task = data.task; });
  }

}
