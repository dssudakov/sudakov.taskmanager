import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { AuthService } from "../auth/auth.service";

@Component({
  templateUrl: './login.component.html',
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {

  login: string;
  password: string;
  errorMessage: string;

  constructor(private authService: AuthService) { }

  onSubmit(): void {

    this.errorMessage = null;
    this.authService.login(this.login, this.password).subscribe(
      next => { },
      error => {
        this.errorMessage = error.status === 401 ? "Invalid login or password" : "Login failed";
      });
  }

  ngOnInit() {
  }

}
