import { Component, AfterViewInit } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, NavigationCancel } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements AfterViewInit {

  isLoading: boolean;

  constructor(private router: Router) {
    this.isLoading = true;
  }

  ngAfterViewInit(): void {

    this.router.events.subscribe(ev => {
      if (ev instanceof NavigationStart) {
        this.isLoading = true;
      }
      else if (ev instanceof NavigationEnd || ev instanceof NavigationCancel) {
        this.isLoading = false;
      }
    });
  }
}
