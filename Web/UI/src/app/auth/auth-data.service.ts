import { Injectable } from "@angular/core";

import { AuthData } from "./auth-data";

@Injectable()
export class AuthDataService {
  private authDataKey: string = "authData";
  private authData: AuthData = this.getSessionAuthData();

  registerLogin(login: string, token: string): void {
    this.authData.isAuth = true;
    this.authData.login = login;
    this.authData.token = token;
    sessionStorage.setItem(this.authDataKey, JSON.stringify(this.authData));
  }

  registerLogout(): void {
    this.authData.isAuth = false;
    this.authData.login = null;
    this.authData.token = null;
    sessionStorage.removeItem(this.authDataKey);
  }

  getAuthData(): AuthData {
    return this.authData;
  }

  private getSessionAuthData(): AuthData {
    var authDataSessionItem = sessionStorage.getItem(this.authDataKey);
    return authDataSessionItem ? JSON.parse(authDataSessionItem) : new AuthData();
  }
}
