import { InjectionToken } from "@angular/core";

export const AUTH_OPTIONS_TOKEN = new InjectionToken<AuthOptions>("AuthOptions");
