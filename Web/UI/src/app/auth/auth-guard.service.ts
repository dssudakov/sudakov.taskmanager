import { Injectable, Inject } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";

import { AUTH_OPTIONS_TOKEN } from "./auth-options-token";
import { AuthDataService } from "./auth-data.service";

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private authDataService: AuthDataService,
    @Inject(AUTH_OPTIONS_TOKEN) private config: AuthOptions) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    if (this.authDataService.getAuthData().isAuth) {
      return true;
    }

    this.authDataService.registerLogout();
    this.router.navigate([this.config.loginUrl]);
    return false;
  }

}
