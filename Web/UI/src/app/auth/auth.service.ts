import { Injectable, Inject } from "@angular/core";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { Observable, config } from "rxjs";
import { tap } from 'rxjs/operators';

import { AUTH_OPTIONS_TOKEN } from "./auth-options-token";
import { AuthData } from "./auth-data";
import { AuthDataService } from "./auth-data.service";

@Injectable()
export class AuthService {

  constructor(
    private router: Router,
    private http: HttpClient,
    private authDataService: AuthDataService,
    @Inject(AUTH_OPTIONS_TOKEN) private config: AuthOptions) {
  }

  login(login: string, password: string): Observable<any> {

    return this.http.post(this.config.tokenUrl, { login: login, password: password }, { responseType: "text" })
      .pipe(tap(data => {
        this.authDataService.registerLogin(login, data);
        this.router.navigate([this.config.mainUrl]);
      }));
  }

  logout(): void {
    this.authDataService.registerLogout();
    this.router.navigate([this.config.loginUrl]);
  }

  getAuthData(): AuthData {
    return this.authDataService.getAuthData();
  }

}
