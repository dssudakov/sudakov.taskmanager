import { Injectable, Inject } from "@angular/core";
import { Router } from "@angular/router";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { tap, catchError } from "rxjs/operators";

import { AUTH_OPTIONS_TOKEN } from "./auth-options-token";
import { AuthDataService } from "./auth-data.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private router: Router,
    private authDataService: AuthDataService,
    @Inject(AUTH_OPTIONS_TOKEN) private config: AuthOptions) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    req = req.clone({
      setHeaders: {
        "Authorization": `Bearer ${this.authDataService.getAuthData().token}`
      }
    });
    return next.handle(req).pipe(
      catchError(response => {
        if (response.status === 401) {
          this.authDataService.registerLogout();
          this.router.navigate([this.config.loginUrl]);
        }

        return throwError(response);
      }));
  }

}
