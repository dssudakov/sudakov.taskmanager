export class AuthData {
  isAuth: boolean;
  login: string;
  token: string;
}
