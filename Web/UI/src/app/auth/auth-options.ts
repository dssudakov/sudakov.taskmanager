interface AuthOptions {
  tokenUrl: string,
  loginUrl: string,
  mainUrl: string
}
