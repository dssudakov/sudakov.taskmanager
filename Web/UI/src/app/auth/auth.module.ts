import { NgModule } from "@angular/core";
import { ModuleWithProviders } from "@angular/compiler/src/core";
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AUTH_OPTIONS_TOKEN } from "./auth-options-token";
import { AuthDataService } from "./auth-data.service";
import { AuthService } from "./auth.service";
import { AuthInterceptor } from "./auth-interceptor.service";
import { AuthGuard } from "./auth-guard.service";

@NgModule({
  imports: [
    HttpClientModule
  ]
})
export class AuthModule {
  static forRoot(config: AuthOptions): ModuleWithProviders {
    return {
      ngModule: AuthModule,
      providers: [
        AuthDataService,
        AuthService,
        AuthGuard,
        { provide: AUTH_OPTIONS_TOKEN, useValue: config },
        { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
      ]
    }
  }
}
