﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Sudakov.TaskManager.Domain.Tasks;

namespace Sudakov.TaskManager.Repositories.Sql.EF.Configurations
{
    public class TaskConfiguration : IEntityTypeConfiguration<Task>
    {
        public void Configure(EntityTypeBuilder<Task> builder)
        {
            builder.ToTable("Tasks");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).HasColumnName("Id");
            builder.Property(x => x.UserId).HasColumnName("UserId");
            builder.Property(x => x.Title).HasColumnName("Title");
            builder.Property(x => x.Description).HasColumnName("Description");
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate");
            builder.Property(x => x.CompleteDate).HasColumnName("CompleteDate");
            builder.Property(x => x.ChangeDate).HasColumnName("ChangeDate");
        }
    }
}
