﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Sudakov.TaskManager.Domain.Users;

namespace Sudakov.TaskManager.Repositories.Sql.EF.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).HasColumnName("Id");
            builder.Property(x => x.Login).HasColumnName("Login");
            builder.Property(x => x.PasswordHash).HasColumnName("PasswordHash");
            builder.Property(x => x.CreateDate).HasColumnName("CreateDate");
            builder.Property(x => x.ChangeDate).HasColumnName("ChangeDate");
        }
    }
}
