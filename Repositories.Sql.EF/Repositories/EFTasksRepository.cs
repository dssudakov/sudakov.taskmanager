﻿using Sudakov.TaskManager.Domain.Tasks;
using Sudakov.TaskManager.Repositories.Sql.EF.UnitOfWork;
using System.Linq;

namespace Sudakov.TaskManager.Repositories.Sql.EF.Repositories
{
    public class EFTasksRepository : EFRepositoryBase<Task>, ITasksRepository
    {
        public EFTasksRepository(EFUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IQueryable<Task> FindAll(int userId)
        {
            return GetQuery().Where(x => x.UserId == userId);
        }
    }
}
