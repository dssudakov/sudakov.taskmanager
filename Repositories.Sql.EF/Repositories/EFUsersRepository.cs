﻿using Sudakov.TaskManager.Domain.Users;
using Sudakov.TaskManager.Repositories.Sql.EF.UnitOfWork;
using System.Linq;

namespace Sudakov.TaskManager.Repositories.Sql.EF.Repositories
{
    public class EFUsersRepository : EFRepositoryBase<User>, IUsersRepository
    {
        public EFUsersRepository(EFUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public User FindBy(string login)
        {
            return GetQuery().FirstOrDefault(x => x.Login == login);
        }
    }
}
