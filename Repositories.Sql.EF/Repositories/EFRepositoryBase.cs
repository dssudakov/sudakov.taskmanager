﻿using Microsoft.EntityFrameworkCore;
using Sudakov.TaskManager.Infrastructure.DomainBase;
using Sudakov.TaskManager.Infrastructure.Repositories;
using Sudakov.TaskManager.Repositories.Sql.EF.UnitOfWork;
using System.Linq;

namespace Sudakov.TaskManager.Repositories.Sql.EF.Repositories
{
    public abstract class EFRepositoryBase<TEntity> : RepositoryBase<TEntity>
        where TEntity : class, IAggregateRoot
    {
        protected DbContext DbContext { get; private set; }

        public EFRepositoryBase(EFUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
            DbContext = unitOfWork.DbContext;
        }

        public override IQueryable<TEntity> FindAll()
        {
            return GetQuery();
        }

        public override TEntity FindBy(int key)
        {
            return DbContext.Find<TEntity>(key);
        }

        protected virtual IQueryable<TEntity> GetQuery()
        {
            return DbContext.Set<TEntity>().AsQueryable();
        }
    }
}
