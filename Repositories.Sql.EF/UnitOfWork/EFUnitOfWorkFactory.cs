﻿using Microsoft.EntityFrameworkCore;
using Sudakov.TaskManager.Infrastructure.UnitOfWork;

namespace Sudakov.TaskManager.Repositories.Sql.EF.UnitOfWork
{
    public class EFUnitOfWorkFactory : IUnitOfWorkFactory
    {
        private readonly DbContextOptions _dbContextOptions;

        public EFUnitOfWorkFactory(string connectionString)
        {
            _dbContextOptions = new DbContextOptionsBuilder().UseSqlServer(connectionString).Options;
        }

        public IUnitOfWork Create()
        {
            return new EFUnitOfWork(new AppDbContext(_dbContextOptions));
        }
    }
}
