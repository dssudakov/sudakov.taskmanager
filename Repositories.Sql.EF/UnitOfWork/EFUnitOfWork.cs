﻿using Microsoft.EntityFrameworkCore;
using Sudakov.TaskManager.Domain.Tasks;
using Sudakov.TaskManager.Domain.Users;
using Sudakov.TaskManager.Infrastructure.DomainBase;
using Sudakov.TaskManager.Infrastructure.Repositories;
using Sudakov.TaskManager.Infrastructure.UnitOfWork;
using Sudakov.TaskManager.Repositories.Sql.EF.Repositories;
using System;
using System.Collections.Generic;

namespace Sudakov.TaskManager.Repositories.Sql.EF.UnitOfWork
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private static readonly IDictionary<Type, Func<EFUnitOfWork, IRepository>> _repositoryFactories;
        private DbContext _dbContext;
        private bool _isDisposed;

        public DbContext DbContext => _dbContext;

        static EFUnitOfWork()
        {
            _repositoryFactories = new Dictionary<Type, Func<EFUnitOfWork, IRepository>>();
            _repositoryFactories.Add(typeof(IUsersRepository), x => new EFUsersRepository(x));
            _repositoryFactories.Add(typeof(ITasksRepository), x => new EFTasksRepository(x));
        }

        public EFUnitOfWork(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public T GetRepository<T>()
        {
            var repositoryType = typeof(T);
            if (_repositoryFactories.ContainsKey(repositoryType))
            {
                return (T)_repositoryFactories[repositoryType](this);
            }

            throw new InvalidOperationException("Unsupported repopsitory type");
        }

        public void RegisterAdded(IAggregateRoot entity)
        {
            _dbContext.Add(entity);
        }

        public void RegisterChanged(IAggregateRoot entity)
        {
            _dbContext.Update(entity);
        }

        public void RegisterRemoved(IAggregateRoot entity)
        {
            _dbContext.Remove(entity);
        }

        public void Commit()
        {
            _dbContext.SaveChanges();
        }

        public void Rollback()
        {
            throw new NotSupportedException();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (!_isDisposed)
            {
                if (isDisposing)
                {
                    _dbContext.Dispose();
                }
            }
            _isDisposed = true;
        }
    }
}
