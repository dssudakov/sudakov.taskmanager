﻿using Microsoft.EntityFrameworkCore;
using Sudakov.TaskManager.Domain.Tasks;
using Sudakov.TaskManager.Domain.Users;
using Sudakov.TaskManager.Repositories.Sql.EF.Configurations;

namespace Sudakov.TaskManager.Repositories.Sql.EF
{
    public class AppDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Task> Tasks { get; set; }

        public AppDbContext(DbContextOptions options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new TaskConfiguration());
        }
    }
}
